#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 17 14:06:38 2022

@author: julian

Seleccionar los puntos extremos del tablero a reconocer. 
Por defecto 4 puntos.
Deben seleccionarse en sentido horario empezando del punto superior izquierdo.
"""

import cv2
import numpy as np
import os 
ix,iy = -1,-1
img = 0 

###################################################################################
## 
###################################################################################

def draw_circle(event,x,y,flags,param):
    global ix,iy
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.circle(img,(x,y),2,(255,0,0),-1)
        ix,iy = x,y

def obtener_puntos(image,n=4):
    global img 
    img = image.copy()
    img = cv2.resize(img,(600,600))
    width, height = image.shape[:2]
    cv2.namedWindow("image")
    cv2.setMouseCallback("image",draw_circle)
    points = []
    print("Press a para agregar punto: ")
    while len(points) != n:
        cv2.imshow("image",img)
        k = cv2.waitKey(1)
        if k == ord('a'):
            points.append([int(ix),int(iy)])
            cv2.circle(img,(ix,iy),6,(0,0,255),-1)
    cv2.destroyAllWindows()
    return list(points)
