WhitePieces = ["King", "Queen", "Bishop_1", "Bishop_2", "Knight_1", "Knight_2", "Rook_1", "Rook_2",
               "Pawn_1", "Pawn_2", "Pawn_3", "Pawn_4", "Pawn_5", "Pawn_6", "Pawn_7", "Pawn_8"]

BlackPieces = ["King", "Queen", "Bishop_1", "Bishop_2", "Knight_1", "Knight_2", "Rook_1", "Rook_2",
               "Pawn_1", "Pawn_2", "Pawn_3", "Pawn_4", "Pawn_5", "Pawn_6", "Pawn_7", "Pawn_8"]

OccupiedSquares = {

        'White': {(1, 2): "Pawn",   (2, 2): "Pawn",   (3, 2): "Pawn",   (4, 2): "Pawn",
                  (5, 2): "Pawn",   (6, 2): "Pawn",   (7, 2): "Pawn",   (8, 2): "Pawn",
                  (1, 1): "Rook",   (8, 1): "Rook",   (2, 1): "Knight", (7, 1): "Knight",
                  (3, 1): "Bishop", (6, 1): "Bishop", (4, 1): "Queen",  (5, 1): "King"},

        'Black': {(1, 7): "Pawn",   (2, 7): "Pawn",   (3, 7): "Pawn",   (4, 7): "Pawn",
                  (5, 7): "Pawn",   (6, 7): "Pawn",   (7, 7): "Pawn",   (8, 7): "Pawn",
                  (1, 8): "Rook",   (8, 8): "Rook",   (2, 8): "Knight", (7, 8): "Knight",
                  (3, 8): "Bishop", (6, 8): "Bishop", (4, 8): "Queen",  (5, 8): "King"}

                   }

squares = {}
corners = []
points = []

rectified_squares = {}
rectified_corners = []
rectified_points = []


def occupied_squares():
    """Return black and white occupied squares."""

    t_dic = {}
    for dic in OccupiedSquares.values():
        t_dic.update(dic)
    return t_dic
#%%
import numpy as np
import cv2
import yaml

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((7*7,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:7].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

cap = cv2.VideoCapture(0)
found = 0
while(found < 10):  # Here, 10 can be changed to whatever number you like to choose
    ret, img = cap.read() # Capture frame-by-frame
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (7,7),None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)   # Certainly, every loop objp is the same, in 3D.
        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        imgpoints.append(corners2)

        # Draw and display the corners
        img = cv2.drawChessboardCorners(img, (7,7), corners2, ret)
        found += 1

    cv2.imshow('img', img)
    cv2.waitKey(10)

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

# It's very important to transform the matrix to list.

data = {'camera_matrix': np.asarray(mtx).tolist(), 'dist_coeff': np.asarray(dist).tolist()}

with open("calibration.yaml", "w") as f:
    yaml.dump(data, f)
    
#%%

import cv2 
import numpy as np 

img = cv2.imread('11.jpg') 
img = cv2.resize(img, (600, 600))


# Puntos internos del tablero
def Corners (img):
    
    # Segmentación por color para obtener una máscara binaria
    lwr = np.array([0, 0, 143])
    upr = np.array([179, 61, 252])
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    msk = cv2.inRange(hsv, lwr, upr)
    
    # Extraer tablero de ajedrez
    krn = cv2.getStructuringElement(cv2.MORPH_RECT, (50, 30))
    dlt = cv2.dilate(msk, krn, iterations=5)
    res = 255 - cv2.bitwise_and(dlt, msk)
    
    # Visualización de las características del tablero de ajedrez
    res = np.uint8(res)
    ret, corners = cv2.findChessboardCorners(res, (7, 7),
                                             flags=cv2.CALIB_CB_ADAPTIVE_THRESH +
                                             cv2.CALIB_CB_FAST_CHECK +
                                             cv2.CALIB_CB_NORMALIZE_IMAGE)

    if ret:
        fnl = cv2.drawChessboardCorners(img, (7, 7), corners, ret)
        cv2.imshow("fnl", fnl)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    else:
        print("No se encontró ningún tablero de ajedrez")
    return (corners)

corners = Corners(img)


# Tomo 4 puntos del tablero respecto de los puntos internos.
pt_B = [(corners[0,0,0])+70, (corners[0,0,1])+50]
pt_C = [(corners[6,0,0])-70, (corners[6,0,1])+50]
pt_A = [(corners[42,0,0])+70, (corners[42,0,1])-50]
pt_D = [(corners[48,0,0])-70, (corners[48,0,1])-50]

# Here, I have used L2 norm. You can use L1 also.
width_AD = np.sqrt(((pt_A[0] - pt_D[0]) ** 2) + ((pt_A[1] - pt_D[1]) ** 2))
width_BC = np.sqrt(((pt_B[0] - pt_C[0]) ** 2) + ((pt_B[1] - pt_C[1]) ** 2))
maxWidth = max(int(width_AD), int(width_BC))

height_AB = np.sqrt(((pt_A[0] - pt_B[0]) ** 2) + ((pt_A[1] - pt_B[1]) ** 2))
height_CD = np.sqrt(((pt_C[0] - pt_D[0]) ** 2) + ((pt_C[1] - pt_D[1]) ** 2))
maxHeight = max(int(height_AB), int(height_CD))

input_pts = np.float32([pt_A, pt_B, pt_C, pt_D])
output_pts = np.float32([[0, 0],
                        [0, maxHeight - 1],
                        [maxWidth - 1, maxHeight - 1],
                        [maxWidth - 1, 0]])

# Compute the perspective transform M
M = cv2.getPerspectiveTransform(input_pts,output_pts)

out = cv2.warpPerspective(img,M,(maxWidth, maxHeight),flags=cv2.INTER_LINEAR)

out[0:46,0:58]=130
out[0:46,60:118]=140
out[0:46,120:118+58]=150
out[0:46,118+58+2:118+58+58+2]=160

cv2.imshow("out", out)
cv2.waitKey(0)
cv2.destroyAllWindows()
#%%

import cv2 
import numpy as np 

img = cv2.imread('tabarriba.jpg') 
img = cv2.resize(img, (700, 800))

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

umbral,imagenmetodo = cv2.threshold(gray,0,255,cv2.THRESH_OTSU)
mascara = np.uint8((gray<umbral)*255)

num_labels,labels,stats,centroids=cv2.connectedComponentsWithStats(mascara,4,cv2.CV_32S)

valor_max_pix=(np.max(stats[:4][1:]))/2
pin = np.where((stats[:,4][1:])>valor_max_pix)
pin = pin[0]+1

print("hay "+ str(len(pin))+ " objetos")
cv2.imshow("gray", mascara)
cv2.waitKey(0)
cv2.destroyAllWindows()
mascaras =[]
mascarasfinal=0

for i in range (0,len(pin)):
    mascara=pin[i]==labels
    mascaras.append(mascara)
    mascarasfinal = mascarasfinal +mascaras[i]
    
#%%

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 26 17:10:54 2022

@author: julian
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt
img = cv2.imread('11.jpg',0)
img = cv2.resize(img, (600, 600))

kernel = np.ones((4,4),np.uint8)

gradient = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, kernel)

kernel = np.ones((40,40),np.uint8)

erosion = cv2.erode(img, kernel,iterations = 1)
dilatado = cv2.dilate(erosion, kernel, iterations = 1)


cv2.imshow("gradient", gradient)
cv2.imshow("erosion", erosion)
cv2.imshow("dilatado", dilatado)


cv2.waitKey(0)
cv2.destroyAllWindows()

gray = dilatado
 
ret,thresh  = cv2.threshold(gray,140,255,cv2.THRESH_BINARY)


# Remove some small noise if any.
dilate = cv2.dilate(thresh,None) 
erode = cv2.erode(dilate,None) 

# Find contours with cv2.RETR_CCOMP 
contours,hierarchy = cv2.findContours(erode,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE) 

for i,cnt in enumerate(contours): 
    # Check if it is an external contour and its area is more than 100 
    if hierarchy[0,i,3] == -1 and cv2.contourArea(cnt)>100: 
        x,y,w,h = cv2.boundingRect(cnt) 
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2) 
        
        m = cv2.moments(cnt) 
        cx,cy = m['m10']/m['m00'],m['m01']/m['m00'] 
        cv2.circle(img,(int(cx),int(cy)),3,255,-1) 
   

cv2.imshow('thresh',thresh) 
cv2.imshow('dilate',dilate) 
cv2.imshow('erode ',erode) 
cv2.imshow('img',img) 
cv2.waitKey(0) 
cv2.destroyAllWindows()
#%%

import cv2 
import numpy as np 
# Normal routines 
img = cv2.imread('31.jpg') 
img = cv2.resize(img, (600, 600))
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY) 
ret,thresh = cv2.threshold(gray,50,255,1) 
# Remove some small noise if any.
dilate = cv2.dilate(thresh,None) 
erode = cv2.erode(dilate,None) 
# Find contours with cv2.RETR_CCOMP 
contours,hierarchy = cv2.findContours(erode,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE) 
for i,cnt in enumerate(contours): 
    # Check if it is an external contour and its area is more than 100 
    if hierarchy[0,i,3] == -1 and cv2.contourArea(cnt)>100: 
        x,y,w,h = cv2.boundingRect(cnt) 
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2) 
        
        m = cv2.moments(cnt) 
        cx,cy = m['m10']/m['m00'],m['m01']/m['m00'] 
        cv2.circle(img,(int(cx),int(cy)),3,255,-1) 
   
cv2.imshow('img',img) 

cv2.waitKey(0) 
cv2.destroyAllWindows()

