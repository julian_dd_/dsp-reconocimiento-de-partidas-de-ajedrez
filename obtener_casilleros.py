#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2 
import numpy as np 

# casilleros = [[],[],[],[]] 
casilleros = []
xs = []
ys = []
ws = []
hs = []



def estimar_casilleros (img):
    '''
    Created on Fri Jul 15 10:36:13 2022

    @author: Julian Delle Donne
    
    Divide en 64 cuadrantes  la imagen
    '''
    casilleros.clear()
    xs.clear()
    ys.clear()
    ws.clear()
    hs.clear()
    
    # crear mascara
    height = img.shape[0]
    width = img.shape[1]
    mask = np.zeros((height ,width), np.uint8)
    
    # divido imagen en 8
    width64 = int(width/8)
    height64 = int(height/8)

    
    for w in range (8):
        for h in range (8):
            mask[h*height64:(h*height64) + height64 ,w*width64: (w*width64) + width64] = 255

            cv2.rectangle(img,(h*height64,w*width64),((h*height64) + height64,(w*width64) + width64),(0,255,0),3)
            xs.append(h*height64)
            ys.append(w*width64)
            ws.append((h*height64) + height64)
            hs.append((w*width64) + width64)
            casilleros.append([h*height64,w*width64,(h*height64) + height64 ,(w*width64) + width64])

                       
    # cv2.imshow("masked ", img )
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    return xs,ys,ws,hs,casilleros

def estimar_boxes(xs,ys,ws,hs,gris):
    '''
    Created on Fri Jul 15 10:36:13 2022

    @author: Julian Delle Donne
    
    Clasifica los 64 cuadrantes de estimar_casilleros()
    '''
    # boxes = np.zeros((8,8),dtype=int)  
    dim1, dim2 = (8, 8) 
    boxes = [[0 for i in range(dim1)] for j in range(dim2)] 
    n=0
        
    for i in range(8):
        k=0
        for j in range(8):
            boxes[i][j] = (xs[k],ys[n],ws[k],hs[n])
            n = n+1
            k = k+1    
        
    img_casilleros = cv2.cvtColor(gris, cv2.COLOR_GRAY2BGR)

    for i in range(8):
        for j in range(8):
            box1 = boxes[i][j]
            cv2.rectangle(img_casilleros, (int(box1[0]), int(box1[1])), (int(box1[2]), int(box1[3])), (255,0,0), 2)
            cv2.putText(img_casilleros,"({},{})".format(i,j),(int(box1[2])-45, int(box1[3])-25),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255),2)
            cv2.imshow("img_casilleros",img_casilleros)
            
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    return boxes


def obtener_casilleros (img):
    
    '''
    Created on Fri Jul 15 10:36:13 2022

    @author: Julian Delle Donne
    
    Dar una imagen en escala de grices preprocesada por ejemplo con Canny.
    Procesa los bordes verticales/ horizontales y en base al area
    devuelve los casilleros del tablero de ajedrez
    '''
    # Elimino las listas, sino cuando se corre mas de una vez conserva los resultados antoriores
    casilleros.clear()
    xs.clear()
    ys.clear()
    ws.clear()
    hs.clear()
    
    gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    # line_min_width = 5
    
    # kernal_h = np.ones((1,line_min_width), np.uint8)
    # img_bin_h = cv2.morphologyEx(gris, cv2.MORPH_OPEN, kernal_h)
    
    # # cv2.imshow("img_bin_h", img_bin_h)
    # # cv2.waitKey(0)
    # # cv2.destroyAllWindows()
    
    # kernal_v = np.ones((line_min_width,1), np.uint8)
    # img_bin_v = cv2.morphologyEx(gris, cv2.MORPH_OPEN, kernal_v)
    
    # # cv2.imshow("img_bin_v", img_bin_v)
    # # cv2.waitKey(0)
    # # cv2.destroyAllWindows()
    
    # img_bin_final=img_bin_h|img_bin_v
    # final_kernel = np.ones((3,3), np.uint8)
    # img_bin_final=cv2.dilate(img_bin_final,final_kernel,iterations=1)
    
    # cv2.imshow("img_bin_final", img_bin_final)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    ret, labels, stats, centroids = cv2.connectedComponentsWithStats(~gris, connectivity=8, ltype=cv2.CV_32S)
    
    ret = imshow_components(labels)
    cv2.imshow("ret", ret)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    

    ### 1 y 0 y los componentes conectados de fondo y residuos que no requerimos
    for x,y,w,h,area in stats[2:]:
        #cv2.putText(img,'box',(x-10,y-10),cv2.FONT_HERSHEY_SIMPLEX, 1.0,(0,255,0), 2)
        if area>1000 and area<3000:
            cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
            xs.append(x)
            ys.append(y)
            ws.append(x+w)
            hs.append(y+h)
            casilleros.append([x,y,x+w,y+h])
            # indices_area.append(np.where(stats == area))

    # c = sorted(casilleros, key=lambda i:((i[0]+i[1]),i[0]))  
    # c = sorted(c, key=lambda i: i [1])
    # # Ordeno casilleros
    xs.sort()
    ys.sort()
    ws.sort()
    hs.sort()      
            
    cv2.imshow("casilleros", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    return stats,xs,ys,ws,hs,casilleros

def imshow_components(labels):
    # Creando una imagen hsv, con un valor de tono único para cada etiqueta
    label_hue = np.uint8(179*labels/np.max(labels))
    # Haciendo que la saturación y el volumen sean 255
    empty_channel = 255*np.ones_like(label_hue)
    labeled_img = cv2.merge([label_hue, empty_channel, empty_channel])
    # Convertir la imagen hsv a imagen BGR
    labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)
    labeled_img[label_hue==0] = 0
    # Devolver la imagen en color para visualizar Connected Componenets
    return labeled_img


def boxes(xs,ys,ws,hs,gris):
    # boxes = np.zeros((8,8),dtype=int)  
    dim1, dim2 = (8, 8) 
    boxes = [[0 for i in range(dim1)] for j in range(dim2)] 
    n=0
        
    for i in range(8):
        k=0
        for j in range(8):
            boxes[i][j] = (xs[k],ys[n],ws[k],hs[n])
            n = n+1
            k = k+8    
        
    cas = cv2.cvtColor(gris, cv2.COLOR_GRAY2BGR)

    for i in range(8):
        for j in range(8):
            box1 = boxes[i][j]
            cv2.rectangle(cas, (int(box1[0]), int(box1[1])), (int(box1[2]), int(box1[3])), (255,0,0), 2)
            cv2.putText(cas,"({},{})".format(i,j),(int(box1[2])-40, int(box1[3])-20),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255),1)
            cv2.imshow("img",cas)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    return boxes
