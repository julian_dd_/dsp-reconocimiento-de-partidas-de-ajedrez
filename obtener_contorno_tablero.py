#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import operator
import numpy as np
from matplotlib import pyplot as plt

def obtener_contorno_tablero(img):
    
    """
    Created on Sat Jul 16 11:04:12 2022

    @author: julian
    """
    
    gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    gaussiano = cv2.GaussianBlur(gris, (5,5), 0)
   
    umbral_minimo = 116
    umbral_maximo = 233
    
    canny = cv2.Canny(gaussiano, umbral_minimo, umbral_maximo)

    (contornos,_) = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
     
    
    max_area = 0
    best_cnt = None
    for cnt in contornos:
        area = cv2.contourArea(cnt)
        if area > 1000:
            if area > max_area:
                max_area = area
                best_cnt = cnt
    
    #cv2.drawContours(img,best_cnt,-1,(0,0,255), 2)
    
    # izquierdo = tuple(best_cnt[best_cnt[:,:,0].argmin()][0])
    # derecho = tuple(best_cnt[best_cnt[:,:,0].argmax()][0])
    # superior = tuple(best_cnt[best_cnt[:,:,1].argmin()][0])
    # inferior = tuple(best_cnt[best_cnt[:,:,1].argmax()][0])

    x,y,w,h = cv2.boundingRect(best_cnt)
    
    #cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),3)
      
    width = 600 # ancho
    height = 600 # alto
    
    pts1 = np.float32([[x,y],[x,y+h],[x+w,y],[x+w,y+h]])                  
    pts2 = np.float32([[0,0],[width,0],[0,height],[width,height]])
    
    M = cv2.getPerspectiveTransform(pts1,pts2)
    
    out = cv2.warpPerspective(img,M,(width, height),flags=cv2.INTER_LINEAR)
    
    return out


def contorno_tablero(img):
    """
    Created on Sat Jul 16 11:04:12 2022

    @author: julian
	Encuentra las 4 esquinas extremas del contorno más grande de la imagen.
    """

    contours, h = cv2.findContours(img.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # Obtener contornos
    contours = sorted(contours, key=cv2.contourArea, reverse=True)  # Ordenar por area, descendiendo
    polygon = contours[0]  # imagen más grande

	# El uso de `operator.itemgetter` con `max` y `min` nos permite obtener el índice del punto
	# Cada punto es una matriz de 1 coordenada, por lo tanto, el captador [0], luego [0] o [1] se usa para obtener x e y respectivamente.

	# Bottom-right tiene el valor más grande (x + y) 
	# Top-left tiene el valor mas pequeño (x + y)
	# Bottom-left tiene el valor mas pequeño (x - y) 
	# Top-right tiene el valor más grande (x - y) 
    bottom_right, _ = max(enumerate([pt[0][0] + pt[0][1] for pt in polygon]), key=operator.itemgetter(1))
    top_left, _ = min(enumerate([pt[0][0] + pt[0][1] for pt in polygon]), key=operator.itemgetter(1))
    bottom_left, _ = min(enumerate([pt[0][0] - pt[0][1] for pt in polygon]), key=operator.itemgetter(1))
    top_right, _ = max(enumerate([pt[0][0] - pt[0][1] for pt in polygon]), key=operator.itemgetter(1))

	# Devuelve una matriz de los 4 puntos usando los índices
    # Cada punto está en su propia matriz de una coordenada
    crop_rect = [polygon[top_left][0], polygon[top_right][0], polygon[bottom_right][0], polygon[bottom_left][0]]

    return crop_rect


def distance_entre_puntos(p1, p2):
	""" 
    Created on Sat Jul 16 11:04:12 2022

    @author: julian
    Retorna la distancia escalar entre dos puntos.
    """
    
	a = p2[0] - p1[0]
	b = p2[1] - p1[1]
	return np.sqrt((a ** 2) + (b ** 2))


def crop_and_warp(img, crop_rect):
	""" 
    Created on Sat Jul 16 11:04:12 2022

    @author: julian
    Cambio de perspectiva de la imagen usando los 4 puntos de contorno_tablero()
    """

	# Rectángulo descrito por los puntos superior izquierdo, superior derecho, inferior derecho e inferior izquierdo
	top_left, top_right, bottom_right, bottom_left = crop_rect[0], crop_rect[1], crop_rect[2], crop_rect[3]

	# Tipo de datos en float32 o `getPerspectiveTransform` arrojará un error
	src = np.array([top_left, top_right, bottom_right, bottom_left], dtype='float32')

	# Obtener el lado más largo en el rectángulo
	side = max([
		distance_entre_puntos(bottom_right, top_right),
		distance_entre_puntos(top_left, bottom_left),
		distance_entre_puntos(bottom_right, bottom_left),
		distance_entre_puntos(top_left, top_right)
	])

	# Describa un cuadrado con un lado de la longitud calculada, esta es la nueva perspectiva a la que queremos deformar
	dst = np.array([[0, 0], [side - 1, 0], [side - 1, side - 1], [0, side - 1]], dtype='float32')

	# Obtiene la matriz de transformación para sesgar la imagen para que se ajuste a un cuadrado comparando los 4 puntos antes y después
	m = cv2.getPerspectiveTransform(src, dst)

	# Realiza la transformación sobre la imagen original
	return cv2.warpPerspective(img, m, (int(side), int(side)))
