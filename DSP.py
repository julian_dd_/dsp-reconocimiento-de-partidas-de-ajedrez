#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 17:46:19 2022

@author: julian
"""

###############################################################################
# Librerias
###############################################################################
import cv2
import numpy as np
import os
import chess # biblioteca de ajedrez para Python, con generación de movimientos, validación de movimientos
import chess.engine

###############################################################################
# Funciones importadas
###############################################################################
from obtener_casilleros import obtener_casilleros
from umbralizado import umbralizado
# from detect_points import get_points
# from read_warp_img import get_warp_img
# from find_position_black import find_current_past_position

###############################################################################
# Declaracion de Variables
###############################################################################
puntos = []     # Contiene los puntos de las esquinas del tablero de ajedrez
casilleros = [[],[],[],[]]   # Contiene el punto superior izquierdo e inferior derecho de los casilleros del tablero de ajedrez
fen_line = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR' # Formato FEN de anotacion
tablero = chess.Board(fen=fen_line) # Objeto de tablero de ajedrez (aun no se que hace)
dir_path = os.path.dirname(os.path.realpath(__file__))+"/numpy_saved" # path of current directory
img_resize = (800,800) # Dimenciones de la imagen
img = []
#engine = chess.engine.SimpleEngine.popen_uci("stockfish-10-win\Windows\stockfish_10_x64.exe") # stockfish engine
chess_board = []   # Almacena matriz del tablero de ajedrez
player_bool_position = []
bool_position = np.zeros((8,8),dtype=int)
number_to_position_map = []
last_move = ""
game_img = ""

###############################################################################
# Calibrar camara
###############################################################################
while True:
    print("Necesita calibrar la posicion de la camara? [y/n] : ",end=" ")
    answer = str(input())
    if answer == "y" or answer == "Y":
        print("Precionar q para salir: ")
        cap = cv2.VideoCapture(2)
        while(cap.isOpened()):
            ret, frame = cap.read()
            img_guardada1 = frame
            if ret==True:
                frame=cv2.flip(frame,1) #invierte el cuadro
                
                #Escribe el cuadro invertido
                cv2.imshow('frame',frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        #Libera todo si la tarea ha terminado 
        cap.release() 
        cv2.destroyAllWindows() 
        break
    elif answer == "n" or answer == "N":
        print("\nEspero que la posición de la cámara ya esté establecida...\n")
        break
    else:
        print("Entrada invalida")

###############################################################################
# Umbralizado
###############################################################################
def nothing(X):
    pass
        
while True:
    print("Necesita calibrar la posicion de la camara? [y/n] : ",end=" ")
    answer = str(input())
    if answer == "y" or answer == "Y":
        print("Precionar q para salir: ")
        cap = cv2.VideoCapture(2)
        cv2.namedWindow("thresold_calibration")
        cv2.createTrackbar("thresold", "thresold_calibration", 0, 255, nothing)
        while(cap.isOpened()):
            t =  cv2.getTrackbarPos("thresold", "thresold_calibration")
            ret, frame = cap.read()
            
            frame = cv2.GaussianBlur(frame,(5,5),0)

            gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            
            mask = np.zeros((gray.shape),np.uint8)

            kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(11,11))

            close = cv2.morphologyEx(gray,cv2.MORPH_CLOSE,kernel1)
            
            matrix,thresold = cv2.threshold(close,t,255,cv2.THRESH_BINARY_INV)   
            img_guardada = thresold 
            if ret==True:
                thresold=cv2.flip(thresold,1) #invierte el cuadro
                
                #Escribe el cuadro 
                cv2.imshow('thresold_calibration',thresold)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        #Libera todo si la tarea ha terminado 
        cap.release() 
        cv2.destroyAllWindows()
        break
    elif answer == "n" or answer == "N":
        print("\nEspero que la posición de la cámara ya esté establecida...\n")
        break
    else:
        print("Entrada invalida")

img = img_guardada1
gray = img_guardada 


res = np.uint8(cv2.normalize(close,close,0,255,cv2.NORM_MINMAX))
res2 = cv2.cvtColor(res,cv2.COLOR_GRAY2BGR)

contour,hier = cv2.findContours(close,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

max_area = 0
best_cnt = None
for cnt in contour:
    area = cv2.contourArea(cnt)
    if area > 1000:
        if area > max_area:
            max_area = area
            best_cnt = cnt

cv2.drawContours(mask,[best_cnt],0,255,-1)
cv2.drawContours(mask,[best_cnt],0,0,2)

rese = cv2.bitwise_and(res,mask)


cv2.imshow("gray",gray)
cv2.imshow("close",close)
cv2.imshow("res",rese)


cv2.waitKey(0)
cv2.destroyAllWindows()


kernelx = cv2.getStructuringElement(cv2.MORPH_RECT,(2,10))

dx = cv2.Sobel(res,cv2.CV_16S,1,0)
dx = cv2.convertScaleAbs(dx)
cv2.normalize(dx,dx,0,255,cv2.NORM_MINMAX)
ret,close = cv2.threshold(dx,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
close = cv2.morphologyEx(close,cv2.MORPH_DILATE,kernelx,iterations = 1)

contour, hier = cv2.findContours(close,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
for cnt in contour:
    x,y,w,h = cv2.boundingRect(cnt)
    if h/w > 9:
        cv2.drawContours(close,[cnt],0,255,-1)
    else:
        cv2.drawContours(close,[cnt],0,0,-1)
close = cv2.morphologyEx(close,cv2.MORPH_CLOSE,None,iterations = 2)
closex = close.copy()

cv2.imshow("closex",closex)


cv2.waitKey(0)
cv2.destroyAllWindows()


kernely = cv2.getStructuringElement(cv2.MORPH_RECT,(10,2))
dy = cv2.Sobel(res,cv2.CV_16S,0,2)
dy = cv2.convertScaleAbs(dy)
cv2.normalize(dy,dy,0,255,cv2.NORM_MINMAX)
ret,close = cv2.threshold(dy,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
close = cv2.morphologyEx(close,cv2.MORPH_DILATE,kernely)

contour, hier = cv2.findContours(close,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
for cnt in contour:
    x,y,w,h = cv2.boundingRect(cnt)
    if w/h > 9:
        cv2.drawContours(close,[cnt],0,255,-1)
    else:
        cv2.drawContours(close,[cnt],0,0,-1)

close = cv2.morphologyEx(close,cv2.MORPH_DILATE,None,iterations = 2)
closey = close.copy()

cv2.imshow("closey",closey)
cv2.waitKey(0)
cv2.destroyAllWindows()

res = cv2.bitwise_and(closex,closey)

cv2.imshow("res ",res )
cv2.waitKey(0)
cv2.destroyAllWindows()

contour, hier = cv2.findContours(res,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
centroids = []
for cnt in contour:
    mom = cv2.moments(cnt)
    (x,y) = int(mom['m10']/mom['m00']), int(mom['m01']/mom['m00'])
    cv2.circle(img,(x,y),4,(0,255,0),-1)
    centroids.append((x,y))
    

    
cv2.imshow("img",img)
cv2.waitKey(0)
cv2.destroyAllWindows()

###############################################################################



#%%

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 09:32:51 2022

@author: julian
"""
import cv2 
import numpy as np 
# calibracion manual de thresold
def nothing(X):
    pass
        
while True:
    print("Necesita calibrar la posicion de la camara? [y/n] : ",end=" ")
    answer = str(input())
    if answer == "y" or answer == "Y":
        print("Precionar q para salir: ")
        cap = cv2.VideoCapture(0)
        cv2.namedWindow("thresold_calibration")
        cv2.createTrackbar("thresold", "thresold_calibration", 0, 255, nothing)
        while(cap.isOpened()):
            t =  cv2.getTrackbarPos("thresold", "thresold_calibration")
            ret, frame = cap.read()
            
            
            matrix,thresold = cv2.threshold(frame,t,255,cv2.THRESH_BINARY_INV)
            
            
            
            if ret==True:
                thresold=cv2.flip(thresold,1) #invierte el cuadro
                
                #Escribe el cuadro 
                cv2.imshow('thresold_calibration',thresold)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        break
    elif answer == "n" or answer == "N":
        print("\nEspero que la posición de la cámara ya esté establecida...\n")
        break
    else:
        print("Entrada invalida")
#Libera todo si la tarea ha terminado 
cap.release() 
cv2.destroyAllWindows()



#%%
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 18:20:02 2022

@author: julian
"""
import cv2 
import numpy as np 
from umbralizado import umbralizado

img11 = cv2.imread('11.jpg')
img = cv2.imread('11.jpg') 
img = cv2.resize(img, (600, 600))

img = cv2.GaussianBlur(img,(5,5),0)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

mask = np.zeros((gray.shape),np.uint8)

kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(11,11))

close = cv2.morphologyEx(gray,cv2.MORPH_CLOSE,kernel1)

res = np.uint8(cv2.normalize(close,close,0,255,cv2.NORM_MINMAX))
res2 = cv2.cvtColor(res,cv2.COLOR_GRAY2BGR)
thresh = cv2.adaptiveThreshold(close,255,0,1,55,1)

#thresh = umbralizado(close,8)

# Tomo contornos
contour,hier = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
# Selecciono el contorno mas grande (tablero)
max_area = 0
best_cnt = None
for cnt in contour:
    area = cv2.contourArea(cnt)
    if area > 1000:
        if area > max_area:
            max_area = area
            best_cnt = cnt

cv2.drawContours(mask,[best_cnt],0,255,-1)
cv2.drawContours(mask,[best_cnt],0,0,2)
# Me quedo con el contorno mas grande (tablero)
rese = cv2.bitwise_and(res,mask)

cv2.imshow("res",rese)
cv2.waitKey(0)
cv2.destroyAllWindows()

kernelx = cv2.getStructuringElement(cv2.MORPH_RECT,(2,10))

dx = cv2.Sobel(res,cv2.CV_16S,1,0)
dx = cv2.convertScaleAbs(dx)
cv2.normalize(dx,dx,0,255,cv2.NORM_MINMAX)
ret,close = cv2.threshold(dx,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
close = cv2.morphologyEx(close,cv2.MORPH_DILATE,kernelx,iterations = 1)

contour, hier = cv2.findContours(close,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
for cnt in contour:
    x,y,w,h = cv2.boundingRect(cnt)
    if h/w > 9:
        cv2.drawContours(close,[cnt],0,255,-1)
    else:
        cv2.drawContours(close,[cnt],0,0,-1)
close = cv2.morphologyEx(close,cv2.MORPH_CLOSE,None,iterations = 2)
closex = close.copy()

cv2.imshow("closex",closex)


cv2.waitKey(0)
cv2.destroyAllWindows()


kernely = cv2.getStructuringElement(cv2.MORPH_RECT,(10,2))
dy = cv2.Sobel(res,cv2.CV_16S,0,1)
dy = cv2.convertScaleAbs(dy)
cv2.normalize(dy,dy,0,255,cv2.NORM_MINMAX)
ret,close = cv2.threshold(dy,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
close = cv2.morphologyEx(close,cv2.MORPH_DILATE,kernely)

contour, hier = cv2.findContours(close,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
for cnt in contour:
    x,y,w,h = cv2.boundingRect(cnt)
    if w/h > 9:
        cv2.drawContours(close,[cnt],0,255,-1)
    else:
        cv2.drawContours(close,[cnt],0,0,-1)

close = cv2.morphologyEx(close,cv2.MORPH_DILATE,None,iterations = 2)
closey = close.copy()

cv2.imshow("closey",closey)
cv2.waitKey(0)
cv2.destroyAllWindows()

res = cv2.bitwise_and(closex,closey)

cv2.imshow("res ",res )
cv2.waitKey(0)
cv2.destroyAllWindows()

contour, hier = cv2.findContours(res,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
centroids = []
for cnt in contour:
    mom = cv2.moments(cnt)
    (x,y) = int(mom['m10']/mom['m00']), int(mom['m01']/mom['m00'])
    cv2.circle(img,(x,y),4,(0,255,0),-1)
    centroids.append((x,y))
    
    
cv2.imshow("img",img)
cv2.waitKey(0)
cv2.destroyAllWindows()

# ordeno los puntos
points = sorted(centroids[:], reverse=True)

# Cambio de perspectiva
width,height = 600,600

# 0 1 -1 -1-1        2 83 77 10
# dd td di   ti     dd di ti  td
pts1 = np.float32([[points[0]],
                 [points[1]],
                 [points[-1]],
                 [points[-1-1]]])
                   
pts2 = np.float32([[0,0],[width,0],[0,height],[width,height]])

M = cv2.getPerspectiveTransform(pts1,pts2)

out = cv2.warpPerspective(img,M,(width, height),flags=cv2.INTER_LINEAR)

cv2.imshow("result",out)
cv2.waitKey(0)
cv2.destroyAllWindows()

# divido imagen en 8
width8 = int(width/8)
height8 = int(height/8)
for w in range (8):
    for h in range (8):
        out[h*height8:(h*height8) + height8 ,w*width8: (w*width8) + width8]=(w+1)*(h+1)*2


cv2.imshow("caja",out)
cv2.waitKey(0)
cv2.destroyAllWindows()


#%%
# Detecciones de los casilleros de ajedrez por canny


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 10:36:13 2022

@author: julian
"""
import cv2 
import numpy as np 

from umbralizado import umbralizado

gray_scale = cv2.imread('11.jpg',0) 
gray_scale = cv2.resize(gray_scale, (600, 600))

img_bin = cv2.Canny(gray_scale,50,110)
dil_kernel = np.ones((3,3), np.uint8)
img_bin=cv2.dilate(img_bin,dil_kernel,iterations=1)


cv2.imshow("img_bin", img_bin)
cv2.waitKey(0)
cv2.destroyAllWindows()

line_min_width = 20

kernal_h = np.ones((1,line_min_width), np.uint8)
img_bin_h = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, kernal_h)

cv2.imshow("img_bin_h", img_bin_h)
cv2.waitKey(0)
cv2.destroyAllWindows()

kernal_v = np.ones((line_min_width,1), np.uint8)
img_bin_v = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, kernal_v)

cv2.imshow("img_bin_v", img_bin_v)
cv2.waitKey(0)
cv2.destroyAllWindows()

img_bin_final=img_bin_h|img_bin_v
final_kernel = np.ones((3,3), np.uint8)
img_bin_final=cv2.dilate(img_bin_final,final_kernel,iterations=1)

cv2.imshow("img_bin_final", img_bin_final)
cv2.waitKey(0)
cv2.destroyAllWindows()


ret, labels, stats,centroids = cv2.connectedComponentsWithStats(~img_bin_final, connectivity=8, ltype=cv2.CV_32S)

def imshow_components(labels):
    ### creating a hsv image, with a unique hue value for each label
    label_hue = np.uint8(179*labels/np.max(labels))
    ### making saturation and volume to be 255
    empty_channel = 255*np.ones_like(label_hue)
    labeled_img = cv2.merge([label_hue, empty_channel, empty_channel])
    ### converting the hsv image to BGR image
    labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)
    labeled_img[label_hue==0] = 0
    ### returning the color image for visualising Connected Componenets
    return labeled_img

ret= imshow_components(labels)
cv2.imshow("ret", ret)
cv2.waitKey(0)
cv2.destroyAllWindows()

### 1 and 0 and the background and residue connected components whihc we do not require
for x,y,w,h,area in stats[2:]:
#     cv2.putText(image,'box',(x-10,y-10),cv2.FONT_HERSHEY_SIMPLEX, 1.0,(0,255,0), 2)
    if area>1000 and area<2500:
        cv2.rectangle(gray_scale,(x,y),(x+w,y+h),(0,255,0),2)


cv2.imshow("gray_scale", gray_scale)
cv2.waitKey(0)
cv2.destroyAllWindows()

#%%

#########################
# Probando 1
#########################

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 17:26:14 2022

@author: julian
"""
###############################################################################
# Librerias
###############################################################################
import cv2
import numpy as np
import os
import chess # biblioteca de ajedrez para Python, con generación de movimientos, validación de movimientos
import chess.engine

###############################################################################
# Funciones importadas
###############################################################################
from obtener_casilleros import obtener_casilleros
from umbralizado import umbralizado


###############################################################################
# Declaracion de Variables
###############################################################################
puntos = []     # Contiene los puntos de las esquinas del tablero de ajedrez
casilleros = [[],[],[],[]]   # Contiene el punto superior izquierdo e inferior derecho de los casilleros del tablero de ajedrez
fen_line = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR' # Formato FEN de anotacion
tablero = chess.Board(fen=fen_line) # Objeto de tablero de ajedrez (aun no se que hace)
dir_path = os.path.dirname(os.path.realpath(__file__))+"/numpy_saved" # path of current directory
img_resize = (800,800) # Dimenciones de la imagen
img = []
#engine = chess.engine.SimpleEngine.popen_uci("stockfish-10-win\Windows\stockfish_10_x64.exe") # stockfish engine
chess_board = []   # Almacena matriz del tablero de ajedrez
player_bool_position = []
bool_position = np.zeros((8,8),dtype=int)
number_to_position_map = []
last_move = ""
game_img = ""

###############################################################################
# Calibrar camara
###############################################################################
while True:
    print("Necesita calibrar la posicion de la camara? [y/n] : ",end=" ")
    answer = str(input())
    if answer == "y" or answer == "Y":
        print("Precionar q para salir: ")
        cap = cv2.VideoCapture(0)
        while(cap.isOpened()):
            ret, frame = cap.read()
            
            if ret==True:
                frame=cv2.flip(frame,1) #invierte el cuadro
                
                #Escribe el cuadro invertido
                cv2.imshow('frame',frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        #Libera todo si la tarea ha terminado 
        cap.release() 
        cv2.destroyAllWindows() 
        break
    elif answer == "n" or answer == "N":
        print("\nEspero que la posición de la cámara ya esté establecida...\n")
        break
    else:
        print("Entrada invalida")

###############################################################################
# Umbralizado
###############################################################################
print("Comenzar operacion de umbralizado:",end=" ")

Frame_GaussianBlur = cv2.GaussianBlur(frame,(5,5),0) # Aplico Filtro de suavizado gaussiano
img_gray = cv2.cvtColor(Frame_GaussianBlur,cv2.COLOR_BGR2GRAY) # Transformo a niveles de grices
mask = np.zeros((img_gray.shape),np.uint8) # Mascara de las mismas dimenciones que la image

kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(11,11))

close = cv2.morphologyEx(img_gray ,cv2.MORPH_CLOSE,kernel1)

threshold = umbralizado(close,16) 


res = np.uint8(cv2.normalize(close,close,0,255,cv2.NORM_MINMAX))
res2 = cv2.cvtColor(res,cv2.COLOR_GRAY2BGR)

contour,hier = cv2.findContours(close,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

max_area = 0
best_cnt = None
for cnt in contour:
    area = cv2.contourArea(cnt)
    if area > 1000:
        if area > max_area:
            max_area = area
            best_cnt = cnt

cv2.drawContours(mask,[best_cnt],0,255,-1)
cv2.drawContours(mask,[best_cnt],0,0,2)

rese = cv2.bitwise_and(res,mask)

cv2.imshow("threshold",threshold)
cv2.imshow("gray",img_gray)
cv2.imshow("close",close)
cv2.imshow("res",rese)

cv2.waitKey(0)
cv2.destroyAllWindows()

kernelx = cv2.getStructuringElement(cv2.MORPH_RECT,(2,10))

dx = cv2.Sobel(res,cv2.CV_16S,1,0)
dx = cv2.convertScaleAbs(dx)
cv2.normalize(dx,dx,0,255,cv2.NORM_MINMAX)
ret,close = cv2.threshold(dx,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
close = cv2.morphologyEx(close,cv2.MORPH_DILATE,kernelx,iterations = 1)

contour, hier = cv2.findContours(close,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
for cnt in contour:
    x,y,w,h = cv2.boundingRect(cnt)
    if h/w > 9:
        cv2.drawContours(close,[cnt],0,255,-1)
    else:
        cv2.drawContours(close,[cnt],0,0,-1)
close = cv2.morphologyEx(close,cv2.MORPH_CLOSE,None,iterations = 2)
closex = close.copy()

cv2.imshow("closex",closex)


cv2.waitKey(0)
cv2.destroyAllWindows()


kernely = cv2.getStructuringElement(cv2.MORPH_RECT,(10,2))
dy = cv2.Sobel(res,cv2.CV_16S,0,2)
dy = cv2.convertScaleAbs(dy)
cv2.normalize(dy,dy,0,255,cv2.NORM_MINMAX)
ret,close = cv2.threshold(dy,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
close = cv2.morphologyEx(close,cv2.MORPH_DILATE,kernely)

contour, hier = cv2.findContours(close,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
for cnt in contour:
    x,y,w,h = cv2.boundingRect(cnt)
    if w/h > 9:
        cv2.drawContours(close,[cnt],0,255,-1)
    else:
        cv2.drawContours(close,[cnt],0,0,-1)

close = cv2.morphologyEx(close,cv2.MORPH_DILATE,None,iterations = 2)
closey = close.copy()

cv2.imshow("closey",closey)
cv2.waitKey(0)
cv2.destroyAllWindows()

res = cv2.bitwise_and(closex,closey)

cv2.imshow("res ",res )
cv2.waitKey(0)
cv2.destroyAllWindows()

contour, hier = cv2.findContours(res,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
centroids = []
for cnt in contour:
    mom = cv2.moments(cnt)
    (x,y) = int(mom['m10']/mom['m00']), int(mom['m01']/mom['m00'])
    cv2.circle(frame,(x,y),4,(0,255,0),-1)
    centroids.append((x,y))
    
    
cv2.imshow("img",frame)
cv2.waitKey(0)
cv2.destroyAllWindows()

# ordeno los puntos
points = sorted(centroids[:], reverse=True)

# Cambio de perspectiva
width,height = 600,600

# 0 1 -1 -1-1        2 83 77 10
# dd td di   ti     dd di ti  td
pts1 = np.float32([[points[0]],
                 [points[1]],
                 [points[-1]],
                 [points[-1-1]]])
                   
pts2 = np.float32([[0,0],[width,0],[0,height],[width,height]])

M = cv2.getPerspectiveTransform(pts1,pts2)

out = cv2.warpPerspective(frame,M,(width, height),flags=cv2.INTER_LINEAR)

cv2.imshow("result",out)
cv2.waitKey(0)
cv2.destroyAllWindows()

# divido imagen en 8
width8 = int(width/8)
height8 = int(height/8)
for w in range (8):
    for h in range (8):
        out[h*height8:(h*height8) + height8 ,w*width8: (w*width8) + width8]=(w+1)*(h+1)*2


cv2.imshow("caja",out)
cv2.waitKey(0)
cv2.destroyAllWindows()


#%%

#########################
# Probando 2
#########################

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 17:26:14 2022

@author: julian
"""
###############################################################################
# Librerias
###############################################################################
import cv2
import numpy as np
import os
import chess # biblioteca de ajedrez para Python, con generación de movimientos, validación de movimientos
import chess.engine

###############################################################################
# Funciones importadas
###############################################################################
from obtener_casilleros import obtener_casilleros
from umbralizado import umbralizado


###############################################################################
# Declaracion de Variables
###############################################################################
puntos = []     # Contiene los puntos de las esquinas del tablero de ajedrez
casilleros = [[],[],[],[]]   # Contiene el punto superior izquierdo e inferior derecho de los casilleros del tablero de ajedrez
fen_line = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR' # Formato FEN de anotacion
tablero = chess.Board(fen=fen_line) # Objeto de tablero de ajedrez (aun no se que hace)
dir_path = os.path.dirname(os.path.realpath(__file__))+"/numpy_saved" # path of current directory
img_resize = (800,800) # Dimenciones de la imagen
img = []
#engine = chess.engine.SimpleEngine.popen_uci("stockfish-10-win\Windows\stockfish_10_x64.exe") # stockfish engine
chess_board = []   # Almacena matriz del tablero de ajedrez
player_bool_position = []
bool_position = np.zeros((8,8),dtype=int)
number_to_position_map = []
last_move = ""
game_img = ""

###############################################################################
# Calibrar camara
###############################################################################
while True:
    print("Necesita calibrar la posicion de la camara? [y/n] : ",end=" ")
    answer = str(input())
    if answer == "y" or answer == "Y":
        print("Precionar q para salir: ")
        cap = cv2.VideoCapture(0)
        while(cap.isOpened()):
            ret, frame = cap.read()
            
            if ret==True:
                frame=cv2.flip(frame,1) #invierte el cuadro
                
                #Escribe el cuadro invertido
                cv2.imshow('frame',frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        #Libera todo si la tarea ha terminado 
        cap.release() 
        cv2.destroyAllWindows() 
        break
    elif answer == "n" or answer == "N":
        print("\nEspero que la posición de la cámara ya esté establecida...\n")
        break
    else:
        print("Entrada invalida")

###############################################################################
# Umbralizado
###############################################################################
print("Comenzar operacion de umbralizado:",end=" ")

Frame_GaussianBlur = cv2.GaussianBlur(frame,(5,5),0) # Aplico Filtro de suavizado gaussiano
img_gray = cv2.cvtColor(Frame_GaussianBlur,cv2.COLOR_BGR2GRAY) # Transformo a niveles de grices
mask = np.zeros((img_gray.shape),np.uint8) # Mascara de las mismas dimenciones que la image

kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(11,11))

close = cv2.morphologyEx(img_gray ,cv2.MORPH_CLOSE,kernel1)

threshold = umbralizado(close,16) 

gray_scale = threshold
gray_scale = cv2.resize(gray_scale, (600, 600))

img_bin = cv2.Canny(gray_scale,50,110)
dil_kernel = np.ones((3,3), np.uint8)
img_bin=cv2.dilate(img_bin,dil_kernel,iterations=1)


cv2.imshow("img_bin", img_bin)
cv2.waitKey(0)
cv2.destroyAllWindows()

line_min_width = 20

kernal_h = np.ones((1,line_min_width), np.uint8)
img_bin_h = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, kernal_h)

cv2.imshow("img_bin_h", img_bin_h)
cv2.waitKey(0)
cv2.destroyAllWindows()

kernal_v = np.ones((line_min_width,1), np.uint8)
img_bin_v = cv2.morphologyEx(img_bin, cv2.MORPH_OPEN, kernal_v)

cv2.imshow("img_bin_v", img_bin_v)
cv2.waitKey(0)
cv2.destroyAllWindows()

img_bin_final=img_bin_h|img_bin_v
final_kernel = np.ones((3,3), np.uint8)
img_bin_final=cv2.dilate(img_bin_final,final_kernel,iterations=1)

cv2.imshow("img_bin_final", img_bin_final)
cv2.waitKey(0)
cv2.destroyAllWindows()


ret, labels, stats,centroids = cv2.connectedComponentsWithStats(~img_bin_final, connectivity=8, ltype=cv2.CV_32S)

def imshow_components(labels):
    ### creating a hsv image, with a unique hue value for each label
    label_hue = np.uint8(179*labels/np.max(labels))
    ### making saturation and volume to be 255
    empty_channel = 255*np.ones_like(label_hue)
    labeled_img = cv2.merge([label_hue, empty_channel, empty_channel])
    ### converting the hsv image to BGR image
    labeled_img = cv2.cvtColor(labeled_img, cv2.COLOR_HSV2BGR)
    labeled_img[label_hue==0] = 0
    ### returning the color image for visualising Connected Componenets
    return labeled_img

ret= imshow_components(labels)
cv2.imshow("ret", ret)
cv2.waitKey(0)
cv2.destroyAllWindows()

### 1 and 0 and the background and residue connected components whihc we do not require
for x,y,w,h,area in stats[2:]:
#     cv2.putText(image,'box',(x-10,y-10),cv2.FONT_HERSHEY_SIMPLEX, 1.0,(0,255,0), 2)
    if area>1000 and area<2500:
        cv2.rectangle(gray_scale,(x,y),(x+w,y+h),(0,255,0),2)


cv2.imshow("gray_scale", gray_scale)
cv2.waitKey(0)
cv2.destroyAllWindows()

#%%
# usando las dos funciones obtener tablero y casilleros
import cv2 
import numpy as np 

from obtener_contorno_tablero import obtener_contorno_tablero
from obtener_casilleros import obtener_casilleros
from umbralizado import umbralizado
from puntos_tablero import get_points

width = 600 
height = 600
img_resize = (600,600) 
casilleros = [[],[],[],[]]

img = cv2.imread('11.jpg') 
img = cv2.resize(img, img_resize)
gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

###############################################################################
## Puntos tablero
###############################################################################
print("Quiere seleccionar los puntos del tablero o hacerlo de forma automatica? [m/a] : ",end=" ")
answer = str(input())
if answer == "m" or answer == "M":
    print("Press q for exit : ")
    warp_points = get_points(img,4)
    pts1 = np.float32([[warp_points[0][0],warp_points[0][1]],
                    [warp_points[1][0],warp_points[1][1]],
                    [warp_points[3][0],warp_points[3][1]],
                    [warp_points[2][0],warp_points[2][1]]])
    pts2 = np.float32([[0,0],[width,0],[0,height],[width,height]])
    
    M = cv2.getPerspectiveTransform(pts1,pts2)
    img_warp = cv2.warpPerspective(img,M,(width, height),flags=cv2.INTER_LINEAR)
    
    cv2.imshow("img_warp",img_warp)
    cv2.waitKey(0)
    cv2.destroyAllWindows()       
    k = cv2.waitKey(1)
    if k == ord('q'):
        cv2.destroyAllWindows()     
elif answer == "a" or answer == "A":
    img_contorno = obtener_contorno_tablero(img)
    cv2.imshow("img_contorno", img_contorno)   
else:
    print("Entrada no valida")
        
       
# img_umbralizada = umbralizado(gris,8)
# cv2.imshow("umbralizado", img_umbralizada)


img_casilleros,casilleros = obtener_casilleros(img_warp)
cv2.imshow("img_casilleros", img_casilleros)





cv2.waitKey(0)
cv2.destroyAllWindows()

#%%
import numpy as np
import cv2
import operator
import numpy as np
from matplotlib import pyplot as plt

from obtener_contorno_tablero import obtener_contorno_tablero
from obtener_casilleros import obtener_casilleros
from umbralizado import umbralizado
from umbralizado import canny
from puntos_tablero import get_points

width = 600 
height = 600
img_resize = (600,600) 
casilleros = [[],[],[],[]]
boxes = np.zeros((8,8),dtype=int)  

def distance_between(p1, p2):
	"""Retorna la distancia escalar entre dos puntos"""
	a = p2[0] - p1[0]
	b = p2[1] - p1[1]
	return np.sqrt((a ** 2) + (b ** 2))

img = cv2.imread('11.jpg') 
img = cv2.resize(img, img_resize)
gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


# crear mascara
height = img.shape[0]
width = img.shape[1]
mask = np.zeros((height ,width), np.uint8)

gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

##### Deberia hacerse antes #######
umbral_minimo = 116
umbral_maximo = 233

img_bin = cv2.Canny(gris,umbral_minimo,umbral_maximo)
dil_kernel = np.ones((3,3), np.uint8)
img_bin=cv2.dilate(img_bin,dil_kernel,iterations=1)

# cv2.imshow("img_bin", img_bin)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
img = img_bin

# cv2.imshow("img_casilleros", img)

# cv2.waitKey(0)
# cv2.destroyAllWindows()


contours, h = cv2.findContours(img.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)  # Find contours
contours = sorted(contours, key=cv2.contourArea, reverse=True)  # Sort by area, descending
polygon = contours[0]  # Largest image
   
   	# Use of `operator.itemgetter` with `max` and `min` allows us to get the index of the point
   	# Each point is an array of 1 coordinate, hence the [0] getter, then [0] or [1] used to get x and y respectively.
   
   	# Bottom-right point has the largest (x + y) value
   	# Top-left has point smallest (x + y) value
   	# Bottom-left point has smallest (x - y) value
   	# Top-right point has largest (x - y) value
bottom_right, _ = max(enumerate([pt[0][0] + pt[0][1] for pt in polygon]), key=operator.itemgetter(1))
top_left, _ = min(enumerate([pt[0][0] + pt[0][1] for pt in polygon]), key=operator.itemgetter(1))
bottom_left, _ = min(enumerate([pt[0][0] - pt[0][1] for pt in polygon]), key=operator.itemgetter(1))
top_right, _ = max(enumerate([pt[0][0] - pt[0][1] for pt in polygon]), key=operator.itemgetter(1))

crop_rect = [polygon[top_left][0], polygon[top_right][0], polygon[bottom_right][0], polygon[bottom_left][0]]


top_left, top_right, bottom_right, bottom_left = crop_rect[0], crop_rect[1], crop_rect[2], crop_rect[3]

	# Explicitly set the data type to float32 or `getPerspectiveTransform` will throw an error
src = np.array([top_left, top_right, bottom_right, bottom_left], dtype='float32')

	# Get the longest side in the rectangle
side = max([
		distance_between(bottom_right, top_right),
		distance_between(top_left, bottom_left),
		distance_between(bottom_right, bottom_left),
		distance_between(top_left, top_right)
	])

	# Describe a square with side of the calculated length, this is the new perspective we want to warp to
dst = np.array([[0, 0], [side - 1, 0], [side - 1, side - 1], [0, side - 1]], dtype='float32')

	# Gets the transformation matrix for skewing the image to fit a square by comparing the 4 before and after points
m = cv2.getPerspectiveTransform(src, dst)


out = cv2.warpPerspective(img, m, (int(side), int(side)))

cv2.imshow("out ", out )
cv2.waitKey(0)
cv2.destroyAllWindows()

out = cv2.cvtColor(out, cv2.COLOR_GRAY2BGR)
cas,ret, labels, stats,xs,ys,ws,hs,casilleros = obtener_casilleros (out)

###################################################################################
## Define Boxes
###################################################################################
# Define los casilleros a partir de "points"
# boxes = np.zeros((8,8,4),dtype=int)   
n=0
# for i in range(8):
#     for j in range(8):
#         aux = (xs[n], ys[n])
#         boxes[i][j] = casilleros[n]
#         # np.insert(boxes, [i][j], ys[n], axis=0)
#         n = n + 1
    

# for i in range(8):
#     for j in range(8):
#         boxes[i][j][0] = xs[n]
#         boxes[i][j][1] = ys[n]
#         boxes[i][j][2] = ws[n]
#         boxes[i][j][3] = hs[n]
#         n = n+1

# cas = cv2.cvtColor(cas, cv2.COLOR_GRAY2BGR)

# for i in range(8):
#     for j in range(8):
#         box1 = boxes[i,j]
#         cv2.rectangle(cas, (int(box1[0]), int(box1[1])), (int(box1[2]), int(box1[3])), (0,255,0), 2)
#         cv2.putText(cas,"({},{})".format(i,j),(int(box1[2])-70, int(box1[3])-50),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255),1)
#         cv2.imshow("img",cas)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# LLAMAR A OBTENER CASILLEROS (BOXES)#################################
dim1, dim2 = (8, 8) 
boxes = [[0 for i in range(dim1)] for j in range(dim2)] 


n=0
# k=0
# for j in range(8):
#     output[0][j] = (xs[k],ys[n],ws[k],hs[n])
#     n = n+1
#     k = k+8

# k=0
# for j in range(8):
#     output[1][j] = (xs[k],ys[n],ws[k],hs[n])
#     n = n+1
#     k = k+8
    
# k=0
# for j in range(8):
#     output[2][j] = (xs[k],ys[n],ws[k],hs[n])
#     n = n+1
#     k = k+8


    
for i in range(8):
    k=0
    for j in range(8):
        boxes[i][j] = (xs[k],ys[n],ws[k],hs[n])
        n = n+1
        k = k+8    
    
cas = cv2.cvtColor(cas, cv2.COLOR_GRAY2BGR)

for i in range(8):
    for j in range(8):
        box1 = boxes[i][j]
        cv2.rectangle(cas, (int(box1[0]), int(box1[1])), (int(box1[2]), int(box1[3])), (255,0,0), 2)
        cv2.putText(cas,"({},{})".format(i,j),(int(box1[2])-40, int(box1[3])-20),cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,0,255),1)
        cv2.imshow("img",cas)
cv2.waitKey(0)
cv2.destroyAllWindows()


    