#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 15:04:26 2022

@author: julian
Funcion principal 
"""

###############################################################################
# Librerias
###############################################################################

import numpy as np
import cv2
import chess
import chess.engine

from IPython.display import display, clear_output

from obtener_contorno_tablero import contorno_tablero, crop_and_warp
from obtener_casilleros import obtener_casilleros, boxes, estimar_casilleros, estimar_boxes
from umbralizado import umbralizado, canny
from puntos_tablero import obtener_puntos
from obtener_turno import obtener_turno, centro_contenido,transformacio_gamma

###############################################################################
# Declaracion de Variables
###############################################################################

width = 600 
height = 600
img_resize = (width,height) 
casilleros = [[],[],[],[]]
box = [None]*64 
centro_pieza = []
flag_inicio = 0

# Video de prueba
#video = 'movimientos.mp4'
video = 'P4.mp4'
###############################################################################
# Declaracion de Variables Pablero de Ajedrez
###############################################################################

board = chess.Board()
numero2posicion_map = []
fen_line = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR' 
board = chess.Board(fen=fen_line) 
chess_board = []  

## (0,0)-> (8,a) , (0,1)-> (8,b)...
def map_function():
    map_position = {}
    x,y=0,0
    for i in "87654321":
        for j in "abcdefgh":
            map_position[j+i] = [x,y]
            y = (y+1)%8
        x = (x+1)%8
    return map_position
mapa = map_function()

# Convierto notacion FEN a matriz 
def fen2board(fen_line):
    chess_board = [] 
    posicion_booleana = []
    for row in fen_line.split(' ')[0].split('/'):
        bool_row = []
        chess_row = []
        for cell in list(row):
            if cell.isnumeric():
                for i in range(int(cell)):
                    chess_row.append(str(1))
                    bool_row.append(0)
            else:
                chess_row.append(cell)
                bool_row.append(1)
        chess_board.append(chess_row)
        posicion_booleana.append(bool_row)
    chess_board = np.array(chess_board)
    posicion_booleana = np.array(posicion_booleana)
    return chess_board,posicion_booleana
chess_board,posicion_booleana = fen2board(fen_line)

# Convierto matriz a FEN
def board2fen(chess_board):
    board_array = chess_board
    fen_line = ''
    count = 0
    for i in range(8):
        empty = 0
        for j in range(8):
            if board_array[i][j].isnumeric():
                empty+=1
            else:
                if empty != 0:
                    fen_line+= str(empty)+ str(board_array[i][j])
                    empty = 0
                else:
                    fen_line += str(board_array[i][j])
        if empty != 0:
            fen_line += str(empty)
        if count != 7:
            fen_line += str('/')
            count +=1
    fen_line += " w KQkq - 0 1"
    return fen_line
fen_line = board2fen(chess_board)

# Matriz de posiciones del tablero
def map_function_for_number_2_position():
    str1 = "87654321"
    str2 = "abcdefgh"
    for i in range(8):
        temp=[]
        for j in range(8):
            temp.append(str(str2[j]+str1[i]))
        numero2posicion_map.append(temp)
    return numero2posicion_map
numero2posicion_map = map_function_for_number_2_position()    

###############################################################################
# Calibrar camara
###############################################################################

while True:
    print("¿Necesita calibrar la posicion de la camara? [y/n] : ",end=" ")
    answer = str(input())
    if answer == "y" or answer == "Y":
        print("Precionar q para salir: ")
        cap = cv2.VideoCapture(video)
        while(cap.isOpened()):
            ret, frame = cap.read()          
            if ret==True:
                frame = cv2.resize(frame,(width,height))
                cv2.imshow('frame',frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        #Libera todo si la tarea ha terminado 
        cap.release() 
        cv2.destroyAllWindows() 
        break
    elif answer == "n" or answer == "N":
        print("\nEspero que la posición de la cámara esté calibrada correctamente...\n")
        cap = cv2.VideoCapture(video)
        while(cap.isOpened()):
            ret, frame = cap.read()          
            if ret==True:
                frame = cv2.resize(frame,(width,height))
                break
        #Libera todo si la tarea ha terminado 
        cap.release() 
        cv2.destroyAllWindows() 
        break
    else:
        print("Entrada no valida")

###############################################################################
                        # ESTADO PRE-PROCESAMIENTO #
###############################################################################

print("\nComenzando operacion de umbralizado ...",end="\n")
img_color = frame
img_color = cv2.resize(img_color, img_resize)
img_gris = cv2.cvtColor(img_color, cv2.COLOR_BGR2GRAY)
# Umbralizado
img_canny = canny(img_color)
# Encontrar las cuatro esquinas del contorno del tablero
contorno = contorno_tablero(img_canny)

# copy = img_color.copy()
# cv2.rectangle(copy,(contorno[0][0],contorno[0][1]),(contorno[2][0],contorno[2][1]),(0,0,255),3)
# cv2.imshow("img_color", copy)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
# Recortar y mapear el tablero para que sea un cuadrado perfecto
img_warp_gris = crop_and_warp(img_canny, contorno)
img_warp_color = cv2.cvtColor(img_warp_gris, cv2.COLOR_GRAY2BGR)

cv2.imshow("img_warp_color", img_warp_color)
cv2.waitKey(0)
cv2.destroyAllWindows()

while True:
    print("¿Se encontro correctamente el tablero? [y/n] : ",end=" ")
    answer = str(input())
    if answer == "y" or answer == "Y":
        break
    elif answer == "n" or answer == "N":
        print("\nSeleccione manualmente los puntos utilizando em mouse\n")
        contorno = obtener_puntos(img_color,4)
        # Recortar y mapear el tablero para que sea un cuadrado perfecto
        img_warp_gris = crop_and_warp(img_canny, contorno)
        img_warp_color = cv2.cvtColor(img_warp_gris, cv2.COLOR_GRAY2BGR)      
        break
    else:
        print("Entrada no valida")

# Obtener los casilleros del tablero
print("\nObteniendo casilleros del tablero...",end="\n")
xs,ys,ws,hs,casilleros = estimar_casilleros (img_warp_gris)
box = estimar_boxes(xs,ys,ws,hs,img_warp_gris)
# Area de referencia para calcular cambios en box
area_box = ws[0]*hs[0]

###### Modo dinamico de obtener casilleros ######
# stats,xs,ys,ws,hs,casilleros = obtener_casilleros(img_warp_color)
# box = boxes(xs,ys,ws,hs,img_warp_gris)
# Realizar control de box chequeando que se encontraron 64 casilleros
if (len(casilleros)!= 64):
    raise ValueError('Oops!  El numero de casilleros encontrados es diferente de 64.')
print("\nOperacion de umbralizado finalizada correctamente",end="\n")

###############################################################################
# Comenzar juego
###############################################################################

print("\nComenzar el juego? [y/n]: ",end="\n")
answer = str(input())
if answer == "y" or answer == "Y":
    indice1 = 0
    turno = "blancas"
    board.turn = False # Asignacion para respetar la logica realizada como "Turno anterior de las negras"
    f = open ('Partida.txt','w') # Archivo de salida .py
    f.write('BLANCAS/NEGRAS\n')
    Estado_I = 1
    Estado_0 = 0
    Estado_1 = 0
    Estado_2 = 0
    Transicion_0 = 0
    while 1:
        cap = cv2.VideoCapture(video)
        salida = cv2.VideoWriter('videoSalida.avi',cv2.VideoWriter_fourcc(*'XVID'),20.0,(600,600))
        while(cap.isOpened()):
            ret, frame = cap.read()  
            ###################################################################
                                    # ESTADO INICAL #
            ###################################################################
            if ret == True and Estado_I == 1 :
                display(board)
                frame = cv2.resize(frame,(width,height))
                frame_gris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#************** Posible filtrado para disminuir ruido?
                # frame_gris  = cv2.GaussianBlur(frame_gris ,(3,3),0)
                frame_warp_gris = crop_and_warp(frame_gris, contorno)
                frame_warp_color = cv2.cvtColor(frame_warp_gris, cv2.COLOR_GRAY2BGR)
                
                # Correccion gamma segun el turno
                # El primero es unicamente para el primer turno
                # Los dos siguientes elsif estan desfasados de turno por que en este instante
                # estoy con el turno pasado, y debo corregir el turno siguiente
                if turno == "blancas" and board.turn == False:
                    frame_warp_gris = transformacio_gamma (frame_warp_gris,1.5)
                elif board.turn == False: 
                    frame_warp_gris = transformacio_gamma (frame_warp_gris,1.5)
                elif board.turn == True:
                    frame_warp_gris = transformacio_gamma (frame_warp_gris,0.6)
   
                img_1 = frame_warp_gris
                Estado_I = 0
                Estado_0 = 1
            
            ###################################################################
                                    # ESTADO 0 #
            ###################################################################
            if ret == True and Estado_0 == 1 :
                frame = cv2.resize(frame,(width,height))
                frame_gris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#************** Posible filtrado para disminuir ruido?
                # frame_gris  = cv2.GaussianBlur(frame_gris ,(3,3),0)
                frame_warp_gris = crop_and_warp(frame_gris, contorno)
                frame_warp_color = cv2.cvtColor(frame_warp_gris, cv2.COLOR_GRAY2BGR)

                # Obtener turno
                turno = obtener_turno(frame,frame_warp_color,box,area_box)
                # Condicion para que entre una sola vez
                if turno == "blancas" and board.turn == False:
                    board.turn = True
                    Transicion_0 = 1
                    print ("\nEstan jugando las: ",turno)
                    # Reseteo la cuenta de "mostrar cambios" para realizar bien la diferencia de imagenes                  
                    indice1 = 0 
                    
                if turno == "negras" and board.turn == True:
                    board.turn = False
                    Transicion_0 = 1
                    print ("\nEstan jugando las: ",turno) 
                    # Reseteo la cuenta de "mostrar cambios" para realizar bien la diferencia de imagenes
                    indice1 = 0 

                # Cuando termina de mover (no hay mas movimiento de mano), tomo los cambios   
                # Se esperan x frame para evitar falsos positivos por sombras o similares
                if Transicion_0 == 1 and board.is_checkmate() == False and turno != "blancas" and turno != "negras" and indice1 < 21:                     
                    if indice1 == 20:
                        Estado_1 = 1
                        Estado_0 = 0                
                        # Correccion gamma segun el turno (a diferencia de antes ya tengo bien el turno)
                        if board.turn == True:
                            frame_warp_gris = transformacio_gamma (frame_warp_gris,1.5)     
                        if board.turn == False:
                            frame_warp_gris = transformacio_gamma (frame_warp_gris,0.6)   
                        img_2 = frame_warp_gris
                    indice1 = indice1 + 1
                    
            ###################################################################
                                    # ESTADO 1 #
            ###################################################################        
            if ret == True and Estado_1 == 1 :
                Transicion_0 = 0
                Estado_0 = 0

                dif = cv2.absdiff(img_1,img_2)   
                cv2.imshow('dif',dif)
                #_, th = cv2.threshold(dif,40, 255, cv2.THRESH_BINARY)
                th = umbralizado(dif)
                # Posible filtrado para evitar falsos positivos
                cv2.imshow('th1',th)
                kernel = np.ones((5,5),np.uint8)
                # Erosión seguida de la dilatación. Es útil para eliminar el ruido
                th = cv2.morphologyEx(th, cv2.MORPH_ERODE, kernel)
                th = cv2.morphologyEx(th, cv2.MORPH_DILATE, kernel)
                
                # Llena lagunas
                th = cv2.morphologyEx(th, cv2.MORPH_DILATE, kernel)

                cv2.imshow('th2',th)
                # Busco cambios entre el 10% y 60% del tamaño de un casillero
                cnts, _ = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                if len(cnts) == 2:
                    centro_pieza = []
                    for c in cnts:
                        area = cv2.contourArea(c)
                        if area > (area_box*10)/100 and area < (area_box*60)/100:
                            (x, y, w, h) = cv2.boundingRect(c)
                            centro_pieza.append([x+int(w/2),y+int(h/2)])
                            #cv2.rectangle(th, (x, y), (x + w, y + h), (0, 0, 255), 2)  

                    # Comprobar que el centro de la pieza esta dentro de un cuadrante de box
                    # Esto tiene que ir en una funsion
                    flag = np.zeros((8,8),dtype = int)
                    posicion_diferencia = np.zeros((8,8),dtype = int)
                    for i in range(8):
                        for j in range(8):
                            for mid_point in centro_pieza:
                                if(centro_contenido(box[i][j],mid_point)) and flag[i][j] == 0:
                                    posicion_diferencia[i][j] = 2
                                    flag[i][j] = 1
                                    Estado_2 = 1
                                    Estado_1 = 0
                                    
                if len(cnts) > 2:
                    print("\nPosible problema de falso positivo, intentando solucionarlo:\n")
                    # Filtrado para evitar falsos positivos
                    kernel = np.ones((9,9),np.uint8)
                    # Erosión seguida de la dilatación. Es útil para eliminar el ruido
                    th = cv2.morphologyEx(th, cv2.MORPH_ERODE, kernel)
                    th = cv2.morphologyEx(th, cv2.MORPH_DILATE, kernel)
                    
                    cv2.imshow('th3',th)
                    centro_pieza = []
                    for c in cnts:
                        area = cv2.contourArea(c)
                        if area > (area_box*10)/100 and area < (area_box*60)/100:
                            (x, y, w, h) = cv2.boundingRect(c)
                            centro_pieza.append([x+int(w/2),y+int(h/2)])
                            #cv2.rectangle(th, (x, y), (x + w, y + h), (0, 0, 255), 2)  
                
                    # Comprobar que el centro de la pieza esta dentro de un cuadrante de box
                    # Esto tiene que ir en una funsion
                    flag = np.zeros((8,8),dtype = int)
                    posicion_diferencia = np.zeros((8,8),dtype = int)
                    for i in range(8):
                        for j in range(8):
                            for mid_point in centro_pieza:
                                if(centro_contenido(box[i][j],mid_point)) and flag[i][j] == 0:
                                    posicion_diferencia[i][j] = 2
                                    flag[i][j] = 1
                                    print(centro_pieza)
                                    Estado_2 = 1
                                    Estado_1 = 0
                
                # Si solo hay un camio no se puede distinguir la jugada.
                # De momento termina el juego, ver como hacer para corregir                    
                if len(cnts) < 2:
                    print("\nError: No se encontraron suficientes cambios\n")
                    break
                    

            ###################################################################
                                    # ESTADO 2 #
            ################################################################### 
            if ret == True and Estado_2 == 1 :
                Estado_1 = 0

                _,posicion_booleana = fen2board(fen_line) 
                matriz_aux = posicion_booleana - posicion_diferencia
                print(matriz_aux)

                posicion_pasada = np.where(matriz_aux == -1)
                '''
                Si se encuentran en la matriz_aux dos lugares con el valor -1
                significa ( en principio) que se ha comido una pieza. Se plantea 
                chequear cuando sucede eso e intentar hacer la logica correspondiente.
                Posible problema de no saber quien come a quien.
                
                Sino, se hace la logica normal, con los valores -1 y -2 registrando 
                posicion pasada y actual
                '''
                if len(posicion_pasada[0]) == 2:
                    print("\nIntercambio de piezas\n")
                    posicion_actual = np.where(matriz_aux == -1)
                    # Manipular listas en vez de tuplas 
                    posicion_actual = list(posicion_actual)
                    posicion_actual[1] = [posicion_actual[0][1]]
                    posicion_actual[0] = [posicion_actual[0][0]]
                    posicion_actual = tuple(posicion_actual)
                    # Manipular listas en vez de tuplas 
                    posicion_pasada = list(posicion_pasada)
                    posicion_pasada[0] = [posicion_pasada[1][0]]
                    posicion_pasada[1] = [posicion_pasada[1][1]]
                    posicion_pasada = tuple(posicion_pasada)
                    
                    pieza_jugada = chess_board[posicion_pasada[0][0]][posicion_pasada[1][0]]
                    chess_board[posicion_pasada] = 1
                    chess_board[posicion_actual] = pieza_jugada
                            
                    numero2palabra = numero2posicion_map[int(posicion_pasada[0][0])][int(posicion_pasada[1][0])]
                    numero2palabra+= numero2posicion_map[int(posicion_actual[0][0])][int(posicion_actual[1][0])]
                
                else:
                    posicion_actual = np.where(matriz_aux == -2)
                    # Guardo la inicial de la pieza jugada
                    pieza_jugada = chess_board[posicion_pasada[0][0]][posicion_pasada[1][0]]
                    # Actualizo la matriz
                    chess_board[posicion_pasada] = 1
                    chess_board[posicion_actual] = pieza_jugada
                    # Armo el string de la jugada        
                    numero2palabra = numero2posicion_map[int(posicion_pasada[0][0])][int(posicion_pasada[1][0])]
                    numero2palabra+= numero2posicion_map[int(posicion_actual[0][0])][int(posicion_actual[1][0])]
                    
                   
                # Universal Chess Interface (uci)
                move = chess.Move.from_uci(numero2palabra)
                print (move)
                board.push(move) # Exporta el movimiento a la libreria. Cuidado! cambia de turno automaticamente!!!
                print(board)
                fen_line = board2fen(chess_board)
                # Escribo la jugada en el archivo
                f.write(chess_board[int(posicion_actual[0][0])][int(posicion_actual[1][0])])
                f.write(numero2palabra)
                if board.turn == True:
                    board.turn = False # Cambio el turno por que previamente board.push cambia el turno solo
                    f.write(' \n')
                elif board.turn == False:
                    board.turn = True # Cambio el turno por que previamente board.push cambia el turno solo
                    f.write(' / ')

                # Obtengo posicion para marcar con rectangulos el movimiento
                posicion1 = str(numero2palabra)[0:2]
                posicion2 = str(numero2palabra)[2:4]
                
                box_1_cordinate = mapa[posicion1]
                box_2_cordinate = mapa[posicion2]
                
                posicion1_box = box[box_1_cordinate[0]][box_1_cordinate[1]]
                posicion2_box = box[box_2_cordinate[0]][box_2_cordinate[1]]
                
                cv2.rectangle(frame_warp_color,(posicion1_box[0],posicion1_box[1]),(posicion1_box[2],posicion1_box[3]),(0,0,255),3)
                cv2.rectangle(frame_warp_color,(posicion2_box[0],posicion2_box[1]),(posicion2_box[2],posicion2_box[3]),(0,255,0),3)
                 
                cv2.imshow("frame_warp_color", frame_warp_color)
                # Vuelvo a repetir el Loop
                Estado_2 = 0
                Estado_I = 1
            
            cv2.imshow('frame',frame_warp_color)
            salida.write(frame_warp_color)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
                                   
        #Libera todo si la tarea ha terminado 
        cap.release() 
        salida.release()
        cv2.destroyAllWindows()
        # Cierro archivo
        f.close()
        break



###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################

# print("\nComenzar el juego? [y/n]: ",end="\n")
# answer = str(input())
# if answer == "y" or answer == "Y":
#     indice1 = 0
#     indice2 = 0 # Para que no entre en la primera ronda
#     turno = "blancas"
#     board.turn = False # Asignacion para respetar la logica realizada como "Turno anterior de las negras"
#     f = open ('Partida.txt','w') # Archivo de salida .py
#     f.write('BLANCAS / NEGRAS\n')
#     while 1:
#         cap = cv2.VideoCapture('VID3.mp4')
#         while(cap.isOpened()):
#             ret, frame = cap.read()
#             if ret == True:
#                 frame = cv2.resize(frame,(width,height))
#                 frame_gris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#                 frame_warp_gris = crop_and_warp(frame_gris, contorno)
#                 img_1 = frame_warp_gris
            
#                 # Obtener turno
#                 turno = obtener_turno(frame,frame_warp_gris,box)
#                 # Condicion para que entre una sola vez
#                 if turno == "blancas" and board.turn == False:
#                     board.turn = True  
#                     print ("Estan jugando las: ",turno)
# # Reseteo la cuenta de "mostrar cambios" para realizar bien la diferencia de imagenes                  
#                     indice2 = 0 
                    
#                 if turno == "negras" and board.turn == True:
#                     board.turn = False
#                     print ("Estan jugando las: ",turno) 
# # Reseteo la cuenta de "mostrar cambios" para realizar bien la diferencia de imagenes
#                     indice2 = 0 

#                 # Cuando termina de mover (no hay mas movimiento de mano), tomo los cambios 
#                 # Tuno de las BLANCAS        
#                 if board.turn == True and board.is_checkmate() == False and turno != "blancas" and turno != "negras" and indice2 < 31:
#                     if indice2 == 20:
#                         img_2 = frame_warp_gris          
#                     if indice1 == 20:
#                         dif = cv2.absdiff(img_1,img_2)   
#                         th = umbralizado(dif)
#                         # cv2.imshow('th',th)
#                         # _, th = cv2.threshold(dif,10, 255, cv2.THRESH_BINARY)
#                         cnts, _ = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#                         if len(cnts) >= 2:
#                             centro_pieza = []
#                             for c in cnts:
#                                 area = cv2.contourArea(c)
#                                 if area > 400 and area < 2000:
#                                     (x, y, w, h) = cv2.boundingRect(c)
#                                     centro_pieza.append([x+int(w/2),y+int(h/2)])
#                                     cv2.rectangle(frame_warp_gris, (x, y), (x + w, y + h), (0, 0, 255), 2)            
                                    
#                             # Comprobar que el centro de la pieza esta dentro de un cuadrante de box
#                             # Esto tiene que ir en una funsion
#                             flag = np.zeros((8,8),dtype=int)
#                             posicion_diferencia = np.zeros((8,8),dtype=int)
#                             for i in range(8):
#                                     for j in range(8):
#                                         for mid_point in centro_pieza:
#                                             if(centro_contenido(box[i][j],mid_point)) and flag[i][j]==0:
#                                                 posicion_diferencia[i][j] = 2
#                                                 flag[i][j]=1
#                                                 flag_inicio = 1
                                                
#                         cv2.imshow('Diferencia',dif)
#                     indice2 = indice2 + 1
#                     indice1 = indice1 + 1
                    
#                     if indice2 == 31 and flag_inicio == 1:
#                         _,posicion_booleana = fen2board(board.fen()) #
#                         print(centro_pieza)
#                         matriz_aux = posicion_booleana - posicion_diferencia
#                         print(matriz_aux)
#                         posicion_pasada = np.where(matriz_aux == -1)
#                         posicion_actual = np.where(matriz_aux == -2)
                        
#                         pieza_jugada = chess_board[posicion_pasada[0][0]][posicion_pasada[1][0]]
#                         chess_board[posicion_pasada]=1
#                         chess_board[posicion_actual]=pieza_jugada
                                    
#                         numero2palabra = numero2posicion_map[int(posicion_pasada[0][0])][int(posicion_pasada[1][0])]
#                         numero2palabra+= numero2posicion_map[int(posicion_actual[0][0])][int(posicion_actual[1][0])]
                        
#                         # Escribo la jugada en el archivo
#                         f.write(chess_board[int(posicion_actual[0][0])][int(posicion_actual[1][0])])
#                         f.write(numero2palabra)
#                         f.write(' / ')
                        
                        
#                         # move = chess.Move.from_uci(numero2palabra)
#                         # print (move)
#                         # if move in board.legal_moves:
#                         #     board.push(move)
#                         # else:
#                         #     print ("Movimiento ilegal")
                        
#                         posicion1 = str(numero2palabra)[0:2]
#                         posicion2 = str(numero2palabra)[2:4]
                        
#                         box_1_cordinate = mapa[posicion1]
#                         box_2_cordinate = mapa[posicion2]
                        
#                         posicion1_box = box[box_1_cordinate[0]][box_1_cordinate[1]]
#                         posicion2_box = box[box_2_cordinate[0]][box_2_cordinate[1]]
                        
#                         cv2.rectangle(img_warp_color,(posicion1_box[0],posicion1_box[1]),(posicion1_box[2],posicion1_box[3]),(0,0,255),3)
#                         cv2.rectangle(img_warp_color,(posicion2_box[0],posicion2_box[1]),(posicion2_box[2],posicion2_box[3]),(0,255,0),3)
                          
#                         cv2.imshow("img_warp_color", img_warp_color)
                        
                        
                        
                        
                        
#                 # Tuno de las NEGRAS           
#                 if board.turn == False and board.is_checkmate() == False and turno != "blancas" and turno != "negras" and indice2 < 21:
#                     if indice2 == 20:
#                         img_2 = frame_warp_gris          
#                     if indice1 > 20:
#                         dif = cv2.absdiff(img_1,img_2)   
#                         th = umbralizado(dif)
#                         # cv2.imshow('th',th)
#                         # _, th = cv2.threshold(dif,10, 255, cv2.THRESH_BINARY)
#                         cnts, _ = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#                         if len(cnts) >= 2:
#                             centro_pieza = []
#                             for c in cnts:
#                                 area = cv2.contourArea(c)
#                                 if area > 400 and area < 2000:
#                                     (x, y, w, h) = cv2.boundingRect(c)
#                                     centro_pieza.append([x+int(w/2),y+int(h/2)])
#                                     cv2.rectangle(frame_warp_gris, (x, y), (x + w, y + h), (0, 0, 255), 2)            
                                    
#                             # Comprobar que el centro de la pieza esta dentro de un cuadrante de box
#                             # Esto tiene que ir en una funsion
#                             flag = np.zeros((8,8),dtype=int)
#                             posicion_diferencia = np.zeros((8,8),dtype=int)
#                             for i in range(8):
#                                     for j in range(8):
#                                         for mid_point in centro_pieza:
#                                             if(centro_contenido(box[i][j],mid_point)) and flag[i][j]==0:
#                                                 posicion_diferencia[i][j] = 2
#                                                 flag[i][j]=1
#                                                 flag_inicio = 1
                                                
#                         cv2.imshow('Diferencia',dif)
#                     indice2 = indice2 + 1
#                     indice1 = indice1 + 1
                    
#                     if indice2 == 21 and flag_inicio == 1:
#                         _,posicion_booleana = fen2board(board.fen()) #
#                         print(centro_pieza)
#                         matriz_aux = posicion_booleana - posicion_diferencia
#                         print(matriz_aux)
#                         posicion_pasada = np.where(matriz_aux == -1)
#                         posicion_actual = np.where(matriz_aux == -2)
                        
#                         pieza_jugada = chess_board[posicion_pasada[0][0]][posicion_pasada[1][0]]
#                         chess_board[posicion_pasada]=1
#                         chess_board[posicion_actual]=pieza_jugada
                                
#                         numero2palabra = numero2posicion_map[int(posicion_pasada[0][0])][int(posicion_pasada[1][0])]
#                         numero2palabra+= numero2posicion_map[int(posicion_actual[0][0])][int(posicion_actual[1][0])]
                        
#                         # Escribo la jugada en el archivo
#                         f.write(chess_board[int(posicion_actual[0][0])][int(posicion_actual[1][0])])
#                         f.write(numero2palabra)
#                         f.write(' \n')
                        
                        
#                         # move = chess.Move.from_uci(numero2palabra)
#                         # print (move)
#                         # if move in board.legal_moves:
#                         #     board.push(move)
#                         # else:
#                         #     print ("Movimiento ilegal")
                        

                        
#                         posicion1 = str(numero2palabra)[0:2]
#                         posicion2 = str(numero2palabra)[2:4]
                        
#                         box_1_cordinate = mapa[posicion1]
#                         box_2_cordinate = mapa[posicion2]
                        
#                         posicion1_box = box[box_1_cordinate[0]][box_1_cordinate[1]]
#                         posicion2_box = box[box_2_cordinate[0]][box_2_cordinate[1]]
                        
#                         cv2.rectangle(img_warp_color,(posicion1_box[0],posicion1_box[1]),(posicion1_box[2],posicion1_box[3]),(0,0,255),3)
#                         cv2.rectangle(img_warp_color,(posicion2_box[0],posicion2_box[1]),(posicion2_box[2],posicion2_box[3]),(0,255,0),3)
                         
#                         cv2.imshow("img_warp_color", img_warp_color)
                        
                        
                        
                        
                        
#                 cv2.imshow('frame',frame_warp_gris)
#                 if cv2.waitKey(1) & 0xFF == ord('q'):
#                     break
                
        
#         #Libera todo si la tarea ha terminado 
#         cap.release() 
#         cv2.destroyAllWindows() 
#         # Cierro archivo
#         f.close()
#         break
# else:
#     print("Finalizado")




###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################



