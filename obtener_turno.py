#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import cv2

fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()           # obtener_turno        
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3)) # obtener_turno

def obtener_turno (frame,frame_warp_color,box,area_box):

    """
    Created on Wed Aug 17 18:12: 01 2022
    
    @author: julian
    Obtener el turno de las blancas o negras por deteccion de movimiento de mano
    del lado derecho o izquierdo
    """
    
    turno = None 
    flag = 0
    # Especificamos los puntos extremos del área blancas
    area_pts_b = np.array([[box[6][0][0],box[6][0][1]], [box[6][7][0],box[6][7][1]], [box[7][7][2],box[7][7][3]], [box[7][0][0],box[7][0][1]]])
    imAux_b = np.zeros(shape=(frame_warp_color.shape[:2]), dtype=np.uint8)
    imAux_b = cv2.drawContours(imAux_b, [area_pts_b], -1, (255), -1)
    image_area_b = cv2.bitwise_and(frame_warp_color, frame_warp_color, mask=imAux_b)
    
    fgmask_b = fgbg.apply(image_area_b)
    fgmask_b = cv2.morphologyEx(fgmask_b, cv2.MORPH_OPEN, kernel)
    fgmask_b = cv2.dilate(fgmask_b, None, iterations=2)
    
    cnts_b = cv2.findContours(fgmask_b, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
    for cnt in cnts_b:
        if cv2.contourArea(cnt) > (area_box*1.5):
            turno = "blancas"
            flag = 1
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.rectangle(frame_warp_color, (x,y), (x+w, y+h),(255,255,0), 2)
            color = (0, 255, 0)    
            cv2.drawContours(frame, [area_pts_b], -1, color, 2)
            cv2.putText(frame_warp_color,"BLANCAS",(x+w, y+h),cv2.FONT_HERSHEY_SIMPLEX,0.6,(255,255,0),2)

    # Especificamos los puntos extremos del área negras
    area_pts_n = np.array([[box[0][0][0],box[0][0][1]], [box[0][7][0],box[0][7][1]], [box[1][7][2],box[1][7][3]], [box[1][0][0],box[1][0][1]]])
    imAux_n = np.zeros(shape=(frame_warp_color.shape[:2]), dtype=np.uint8)
    imAux_n = cv2.drawContours(imAux_n, [area_pts_n], -1, (255), -1)
    image_area_n = cv2.bitwise_and(frame_warp_color, frame_warp_color, mask=imAux_n)
      
    fgmask_n = fgbg.apply(image_area_n)
    fgmask_n = cv2.morphologyEx(fgmask_n, cv2.MORPH_OPEN, kernel)
    fgmask_n = cv2.dilate(fgmask_n, None, iterations=2)
    
    cnts_n = cv2.findContours(fgmask_n, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
    for cnt in cnts_n:
        if cv2.contourArea(cnt) > (area_box*1.5) and flag == 0:
            turno = "negras"
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.rectangle(frame_warp_color, (x,y), (x+w, y+h),(0,255,255), 2)
            color = (0, 0, 255)    
            cv2.drawContours(frame, [area_pts_n], -1, color, 2)
            cv2.putText(frame_warp_color,"NEGRAS",(x+w, y+h),cv2.FONT_HERSHEY_SIMPLEX,0.6,(0,255,255),2)
    return turno

def centro_contenido(rect,punto_medio):
    """
    Created on Wed Aug 17 18:12: 01 2022
    
    @author: julian
    Valor booleano si el centro de la pieza detectada esta dentro de un cuadrante de box
    """
    
    logic = rect[0]<punto_medio[0]<rect[2] and rect[1]<punto_medio[1]<rect[3]
    return logic


def obtener_box (frame_warp_color,box,turno):


    return



def transformacio_gamma (frame_warp_gris,gamma):
    
    gamma_table = [np.power(x / 255.0, gamma)*255.0 for x in range(256)] 
    gamma_table = np.round(np.array(gamma_table)).astype(np.uint8) 
    frame_warp_gris = cv2.LUT(frame_warp_gris, gamma_table)

    return frame_warp_gris

