#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import cv2

def umbralizado(img,n=8):
    """
    Created on Fri Jul 15 14:58:04 2022

    @author: julian
    
    Recibe una imagen en niveles de grises y un numero entero, realiza un 
    umbralizado adaptativo por OTSU en n regiones iguales. Por defecto 8 regiones.
    """
    
    # crear mascara
    height = img.shape[0]
    width = img.shape[1]
    mask = np.zeros((height ,width), np.uint8)
    
    # divido imagen en n
    width8 = int(width/n)
    height8 = int(height/n)

    for w in range (n):
        for h in range (n):
            mask[h*height8:(h*height8) + height8 ,w*width8: (w*width8) + width8] = 255
            masked= cv2.bitwise_and(img,mask)
    
            # Otsu's thresholding after Gaussian filtering
            blur = cv2.GaussianBlur(masked,(9,9),0) #define mascara Gaussiana de 5x5
            ret,threshold = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    return threshold

def nothing(x):
    pass

def canny(img):
    """
    Created on Fri Jul 15 14:58:04 2022

    @author: julian
    
    Recibe una imagen en RGB.
    Transforma la imagen a niveles de grices.
    Realiza gradiente de imagen por Canny manual o automatico.
    """
    
    print("Realizar gradiente de Imagen por canny manual o automatico? [m/a] : ",end=" ")
    answer = str(input())
    if answer == "a" or answer == "A":
        print("Presiona q para salir: ")
        
        gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        umbral_minimo = 116
        umbral_maximo = 233
        
        img_canny = cv2.Canny(gris,umbral_minimo,umbral_maximo)
        dil_kernel = np.ones((3,3), np.uint8)
        img_canny=cv2.dilate(img_canny,dil_kernel,iterations=1)

        # cv2.imshow("img_canny", img_canny)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
           
        k = cv2.waitKey(1)
        if k == ord('q'):
            cv2.destroyAllWindows()   
            
    elif answer == "m" or answer == "M":

        gris = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        cv2.namedWindow('canny')
        cv2.createTrackbar('lower','canny', 0, 255, nothing)
        cv2.createTrackbar('upper', 'canny', 0, 255, nothing)        
        cv2.createTrackbar('filtro', 'canny', 0, 255, nothing)
       
        while (1):          
            umbral_minimo = cv2.getTrackbarPos('lower', 'canny')    
            umbral_maximo = cv2.getTrackbarPos('upper', 'canny')   
            line_min_width  = cv2.getTrackbarPos('filtro', 'canny')
          
            img_canny = cv2.Canny(gris,umbral_minimo,umbral_maximo)
            dil_kernel = np.ones((3,3), np.uint8)
            img_canny=cv2.dilate(img_canny,dil_kernel,iterations=1)
            
            # Detector de bordes verticales y horizontales
            kernal_h = np.ones((1,line_min_width), np.uint8)
            img_bin_h = cv2.morphologyEx(img_canny, cv2.MORPH_OPEN, kernal_h)
            
            kernal_v = np.ones((line_min_width,1), np.uint8)
            img_bin_v = cv2.morphologyEx(img_canny, cv2.MORPH_OPEN, kernal_v)
            
            img_bin_final = img_bin_h|img_bin_v #OR
            
            final_kernel = np.ones((3,3), np.uint8)
            img_canny = cv2.dilate(img_bin_final,final_kernel,iterations=1)         

            
            cv2.imshow('canny',img_canny)           
            if (cv2.waitKey(1) & 0xFF == ord('q')):
             break   
        cv2.destroyAllWindows()
    else:
        print("Entrada no valida")
    return img_canny

